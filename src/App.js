import React, { Component } from 'react';
import './App.css';
import HCSeriesChart from './components/highcharts/SeriesChart'
import HCBarChart from './components/highcharts/BarChart'
import HCLineChart from './components/highcharts/LineChart'
import HCScatterChart from './components/highcharts/ScatterChart'

let data1 = [
  {letter: 'A', frequency: .08167},
  {letter: 'B', frequency: .01492},
  {letter: 'C', frequency: .02782},
  {letter: 'D', frequency: .04253},
  {letter: 'E', frequency: .12702},
  {letter: 'F', frequency: .02288},
  {letter: 'G', frequency: .02015},
  {letter: 'H', frequency: .06094},
  {letter: 'I', frequency: .06966},
  {letter: 'J', frequency: .00153},
  {letter: 'K', frequency: .00772},
  {letter: 'L', frequency: .04025},
  {letter: 'M', frequency: .02406},
  {letter: 'N', frequency: .06749},
  {letter: 'O', frequency: .07507},
  {letter: 'P', frequency: .01929},
  {letter: 'Q', frequency: .00095},
  {letter: 'R', frequency: .05987},
  {letter: 'S', frequency: .06327},
  {letter: 'T', frequency: .09056},
  {letter: 'U', frequency: .02758},
  {letter: 'V', frequency: .00978},
  {letter: 'W', frequency: .02360},
  {letter: 'X', frequency: .00150},
  {letter: 'Y', frequency: .01974},
  {letter: 'Z', frequency: .00074}]

  let linechartdata = {
    "series": {
        "yseries": [
            {
                "name": "Widget Visitors",
                "tooltip": "Avg. time spent by users on watching video",
                "avgtime": [
                    16,
                    16,
                    83,
                    31,
                    32,
                    0,
                    29,
                    731,
                    0
                ],
                "visitors": [
                    [735],
                    530,
                    1,
                    1,
                    6,
                    6,
                    23,
                    19,
                    3
                ]
            }
        ],
        "xseries": [
            "08-03-2018",
            "09-03-2018",
            "12-03-2018",
            "13-03-2018",
            "14-03-2018",
            "17-03-2018",
            "19-03-2018",
            "20-03-2018",
            "21-03-2018"
        ],
        "label": [
            "Date",
            "Avg. Time (In Sec)"
        ]
    }
}

let scatterchartdata = {
  "series": {
      "yseries": [
          {
              "name": "Visitors",
              "visitors": [
                  [
                      1,
                      2.04
                  ],
                  [
                      1,
                      3.08
                  ],
                  [
                      1,
                      1.10
                  ],
                  [
                      2,
                      4.00
                  ],
                  [
                      2,
                      1.88
                  ],
                  [
                      3,
                      0
                  ],
                  [
                      3,
                      1.97
                  ],
                  [
                      3,
                      2.97
                  ]
              ]
          }
      ],
      "xseries": [
          "https://widgetin.yupl.us/ureka/Water_Purifiers.html",
          "https://widgetin.yupl.us/echo/NITRD_ECHO.html",
          "https://widgetin.yupl.us/makemytrip/TajHolidayVillage.html",
          "https://widgetin.yupl.us/tcns/FashionShopping.html"
      ],
      "label": [
          "Page",
          "Avg. Time (In Sec)"
      ]
  }
}

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container">
        <div className="row">
          <div className="col"><HCScatterChart data={scatterchartdata}/></div>
        </div>
        <div className="row">
          <div className="col"><HCLineChart data= {linechartdata}/></div>
        </div>
        <div className="row">
          <div className="col"><HCLineChart data= {linechartdata} label={["Date", "User Session"]} domain={"shopclues.com"}/></div>
          <div className="col"><HCSeriesChart data={data1} label={["Date", "User Session"]} domain={"shopclues.com"}/></div>
        </div>
        {/* <div className="row">
          <div className="col"><HCBarChart data={data} label={["Date", "User Session"]} domain={"shopclues.com"}/></div>
          <div className="col"><HCBarChart data={data} label={["Date", "User Session"]} domain={"shopclues.com"}/></div>
        </div> */}
         
         {/* <div className="row">
           <div className="col"><D3BarChart size={[600, 400]} data={data} domain={"shopclues.com"}/></div>
           <div className="col"><D3BarChart size={[600, 400]} data={data} domain={"shopclues.com"}/></div>
         </div> */}
        </div>
      </div>
    );
  }
}
