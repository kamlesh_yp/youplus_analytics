import React, { Component } from 'react'
import Highcharts from 'highcharts'
import ReactHighcharts from 'react-highcharts'
import '../../App.css';

export default class ScatterChart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            config: {
                chart: { type: 'scatter', zoomType: 'xy'},
                credits: false,
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 100,
                    y: 70,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
                    borderWidth: 1
                },
                plotOptions: {
                    scatter: {
                        marker: {
                            radius: 5,
                            states: {
                                hover: {
                                    enabled: true,
                                    lineColor: 'rgb(100,100,100)'
                                }
                            }
                        },
                        states: {
                            hover: {
                                marker: {
                                    enabled: false
                                }
                            }
                        }
                    }
                }
            }
        }
        this._initChart = this._initChart.bind(this)
     }

    componentDidMount = (e) =>  this._initChart()

    _initChart = (e) => {
        let data = this.props.data
        let temp = this.state.config;
        let series = data.series.yseries[0].visitors;
        let xseries = []
        data.series.xseries.forEach(function(ele) {
            var qsRemoved = ele.split("?")[0].split("#")[0];
            xseries.push(qsRemoved.substr(qsRemoved.lastIndexOf('/') + 1))
        });
        window["urls"] = data.series.xseries
        temp.xAxis = { title: { enabled: true, text: data.series.label[0] },
                    startOnTick: true, endOnTick: true, showLastLabel: true, 
                    categories: xseries }
        temp.title = { text: data.series.label[0] + ' vs ' + data.series.label[1]}
        temp.yAxis = { title: { text: data.series.label[1] } }
        //temp.subtitle = { text: 'Shopclues.com' };
        temp.series = [{ name: data.series.yseries[0].name, color: 'rgb(124, 181, 236)', data: series }];
        temp.tooltip = {
            formatter: function() {
                let self = this;
                debugger;
                return "<b>" + data.series.label[0] + "</b>: " + window["urls"][self.point.x] 
                     + "<br/><b>" + data.series.label[1] + "</b>: " + self.point.y
            }
        }

        this.setState({ config : temp });
    }

    render() {
        return <ReactHighcharts config={this.state.config} ref="chart"></ReactHighcharts>;
     }
}