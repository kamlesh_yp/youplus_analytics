import React, { Component } from 'react'
import ReactHighcharts from 'react-highcharts'
import Highcharts from 'highcharts'
import '../../App.css';

export default class LineChart extends Component{
    constructor(props){
        super(props)
        this.state = {
            config : {
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 100,
                    y: 70,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
                    borderWidth: 1
                },
                credits: false
            } 
        }
        this._initChart = this._initChart.bind(this)
     }
     
     componentDidMount = (e) =>  this._initChart()

     _initChart = (e) => {
        let data = this.props.data
        let temp = this.state.config;
        let series = []
        let visitors = []
        data.series.yseries.forEach(function(ele) {
            var ser = {}
            ser.name = ele.name
            ser.data = ele.avgtime
            series.push(ser)
            visitors.push(ele.visitors)
        });
        window["visitors"] = visitors
        temp.series = series;
        temp.yAxis = { title: { text: data.series.label[1] }}
        temp.xAxis = { title: { text: data.series.label[0] }, categories: data.series.xseries }
        temp.title = { text: data.series.label[0] + ' vs ' + data.series.label[1]}
        temp.tooltip = {
            formatter: function() {
                let self = this;
                var ind = 0
                var a = window["visitors"][this.colorIndex]
                this.series.data.forEach(function(ele, index){ if(ele.y === self.y){ ind = index } })
                return (this.colorIndex === 0) ? 
                   'Widget Users: ' +  a[ind] + ' visitors <br/> Avg. Time Spent: ' + this.y + ' seconds':
                   'All Users: ' + a[ind] + '<br/> Avg. Time Spent: ' + this.y
            }
        }
        this.setState({ config : temp });
     }
     
     render() {
        return <ReactHighcharts config={this.state.config} ref="chart"></ReactHighcharts>;
     }
}