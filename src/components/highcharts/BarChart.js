import React, { Component } from 'react'
import ReactHighcharts from 'react-highcharts'
import '../../App.css';

export default class BarChart extends Component{
    constructor(props){
        super(props)
        this.state = {
            config: {
                chart: { type: 'column' },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                credits: false
            }
        }
        this._initChart = this._initChart.bind(this)
     }
     
     componentDidMount = (e) =>  this._initChart()
     
     _initChart = (e) => {
        let data = this.props.data
        let label = this.props.label
        let domain = this.props.domain
        let temp = this.state.config;
        let xData = [];
        let yData = [];
        data.forEach(function(ele) {
            xData.push(ele.letter);
            yData.push(ele.frequency);
        });
        temp.series = [{ data: yData, name: domain }];
        temp.xAxis = { title: { text: label[0] }, categories: xData }
        temp.yAxis = { min: 0, title: { text: label[1] }}
        temp.title = { text: label[0] + ' vs ' + label[1]}
        temp.subtitle = { text: domain }
        this.setState({ config : temp });
     }

     render() {
        return <ReactHighcharts config={this.state.config} ref="chart"></ReactHighcharts>;
     }
}