import React, { Component } from 'react'
import Highcharts from 'highcharts'
import ReactHighcharts from 'react-highcharts'
import '../../App.css';

export default class SeriesChart extends Component{
    constructor(props){
        super(props)
        this.state = {
            config : {
                chart: { zoomType: 'x' },
                legend: { enabled: false },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: { radius: 2 },
                        lineWidth: 1,
                        states: { hover: { lineWidth: 1 }},
                        threshold: null
                    }
                },
                credits: false
            } 
        }
        this._initChart = this._initChart.bind(this)
     }
     
     componentDidMount = (e) =>  this._initChart()

     _initChart = (e) => {
        let data = this.props.data
        let label = this.props.label
        let domain = this.props.domain
        let temp = this.state.config;
        let xData = [];
        let yData = [];
        data.forEach(function(ele) {
            xData.push(ele.letter);
            yData.push(ele.frequency);
        });
        temp.series = [{ type: 'area', data: yData, name: domain  }];
        temp.xAxis = { title: { text: label[0] }, categories: xData }
        temp.yAxis = { min: 0, title: { text: label[1] }}
        temp.title = { text: label[0] + ' vs ' + label[1]}
        temp.subtitle = { text: domain }
        this.setState({ config : temp });
     }
     
     render() {
        return <ReactHighcharts config={this.state.config} ref="chart"></ReactHighcharts>;
     }
}