import React, { Component } from 'react'
import '../../App.css';
import { scaleLinear, scaleBand } from 'd3-scale'
import { max } from 'd3-array'

export default class BarChart extends Component{
    constructor(props){
        super(props)
        this._initChart = this._initChart.bind(this)
     }
     componentDidMount = (e) =>  this._initChart()

     _initChart(){
        let data = this.props.data
        let canvas = this.canvas
        let context = canvas.getContext('2d')
        let margin = {top: 20, right: 20, bottom: 30, left: 40}, width = canvas.width - margin.left - margin.right,
	                  height = canvas.height - margin.top - margin.bottom;
        
        let x = scaleBand().rangeRound([0, width]).padding(0.1).domain(data.map((d) => d.letter));
        let y = scaleLinear().rangeRound([height, 0]).domain([0, max(data, (d) => d.frequency)]);
        context.translate(margin.left, margin.top);

        let yTickCount = 10,
        yTicks = y.ticks(yTickCount),
        yTickFormat = y.tickFormat(yTickCount);

        context.beginPath();
        x.domain().forEach((d) => { context.moveTo(x(d) + x.bandwidth() / 2, height);
            context.lineTo(x(d) + x.bandwidth() / 2, height + 6);});
        context.strokeStyle = "black";
        context.stroke();

        context.textAlign = "center";
        context.textBaseline = "top";
        x.domain().forEach((d) => {
            context.fillText(d, x(d) + x.bandwidth() / 2, height + 6);
        });

        context.beginPath();
        yTicks.forEach((d) => { context.moveTo(0, y(d) + 0.5);
            context.lineTo(-6, y(d) + 0.5);
        });
        context.strokeStyle = "black";
        context.stroke();

        context.textAlign = "right";
        context.textBaseline = "middle";
        yTicks.forEach((d) => {
           context.fillText(yTickFormat(d), -9, y(d));
        });

        context.beginPath();
        context.moveTo(-6.5, 0 + 0.5);
        context.lineTo(0.5, 0 + 0.5);
        context.lineTo(0.5, height + 0.5);
        context.lineTo(-6.5, height + 0.5);
        context.strokeStyle = "black";
        context.stroke();

        context.save();
        context.rotate(-Math.PI / 2);
        context.textAlign = "right";
        context.textBaseline = "top";
        context.font = "bold 10px sans-serif";
        context.fillText("Frequency", -10, 10);
        context.restore();

        context.fillStyle = "#9999ff";
        data.forEach((d) => {
          context.fillRect(x(d.letter), y(d.frequency), x.bandwidth(), height - y(d.frequency));
        });
     }

     render() {
        return <canvas width={this.props.size[0]} height={this.props.size[1]} ref={(el) => { this.canvas = el }} />
     }
}